var product1={name:"Starter",price:"$125",storage:"200GB",files:"20",bandwidth:"2TB"};
var product2={name:"Ultimate",price:"$195",storage:"800GB",files:"Unlimited",bandwidth:"10TB"};
var product3={name:"Professional",price:"$145",storage:"200GB",files:"20",bandwidth:"2TB"};
var products=new Array(product1,product2,product3);
var names=document.getElementsByClassName("pricing-blocks__name");
var price=document.getElementsByClassName("pricing-blocks__price-1");
var storage=document.getElementsByClassName("pricing-blocks__storage-value");
var files=document.getElementsByClassName("pricing-blocks__files-value");
var bandwidth=document.getElementsByClassName("pricing-blocks__bandwidth-value");
for(var i=0;i<products.length;i++){
    names[i].innerHTML=products[i].name;
    price[i].innerHTML=products[i].price;
    storage[i].innerHTML=products[i].storage;
    files[i].innerHTML=products[i].files;
    bandwidth[i].innerHTML=products[i].bandwidth;
    
}