window.onscroll = function() {
    if (menu.classList.contains('header-scroll') && window.pageYOffset < menuHeight) {
      menu.classList.remove('header-scroll');
    } else if (window.pageYOffset > menuHeight) {
      menu.classList.add('header-scroll');
    }
    var home=document.getElementsByClassName("home")[0].clientHeight;
    var about=document.getElementsByClassName("about")[0].clientHeight;
    var services=document.getElementsByClassName("services")[0].clientHeight;
    var video=document.getElementsByClassName("video")[0].clientHeight;
    var portfolio=document.getElementsByClassName("portfolio")[0].clientHeight;
    var team=document.getElementsByClassName("team")[0].clientHeight;
    var stats=document.getElementsByClassName("stats")[0].clientHeight;
    var blocksHeight=home+about+services+video+portfolio+team+stats;
    var steps=document.getElementsByClassName("steps")[0].clientHeight;
    var step1=document.getElementsByClassName("steps-block-1")[0];
    var step2=document.getElementsByClassName("steps-block-2")[0];
    var step3=document.getElementsByClassName("steps-block-3")[0];
    var step4=document.getElementsByClassName("steps-block-4")[0];
    if (window.pageYOffset>blocksHeight&&window.pageYOffset<blocksHeight+steps){
        if($(window).width() < 940){
            step1.style.animationName="swap11";
            step2.style.animationName="swap22";
            step3.style.animationName="swap11";
            step4.style.animationName="swap22";
        }else{
            step1.style.animationName="swap1";
            step2.style.animationName="swap2";
            step3.style.animationName="swap1";
            step4.style.animationName="swap2";
            step1.style.marginLeft="-65%";
            step2.style.marginLeft="65%";
            step3.style.marginLeft="-65%";
            step4.style.marginLeft="65%";
        }
    }
    else{
        step1.removeAttribute("style");        
        step2.removeAttribute("style");        
        step3.removeAttribute("style");        
        step4.removeAttribute("style");        
    }
};
