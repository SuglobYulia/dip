
  var menu = document.getElementsByClassName('header')[0];

  var menuHeight = menu.getBoundingClientRect().bottom + window.pageYOffset;

  window.onscroll = function() {
    if (menu.classList.contains('header-scroll') && window.pageYOffset < menuHeight) {
      menu.classList.remove('header-scroll');
    } else if (window.pageYOffset > menuHeight) {
      menu.classList.add('header-scroll');
    }
  };
var menuMin=document.querySelector(".menu-icon");
menuMin.onclick=function(){
  var menuUl=document.querySelector(".menu-ul");
  var logoMin=document.querySelector(".logo");
  menuUl.hasAttribute("style")? menuUl.removeAttribute("style"):menuUl.style.display="block" ;

  if(logoMin.hasAttribute("style")){
    logoMin.removeAttribute("style");
  }else{
    logoMin.style.height="230px";
    (menu.classList.contains("header-scroll"))?
    logoMin.style.backgroundPositionY="center":
    logoMin.style.backgroundPositionY="12%";
  }
}